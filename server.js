const express = require("express");
const router = require("./routes");
const port = 8081;
server = express();

server.use(express.json());

server.use("/api", router);

server.use("/", (req, res) => {
  res.send({ message: "Server is running" });
});
server.listen(port, () => {
  console.log("Server is running on port " + port);
});
