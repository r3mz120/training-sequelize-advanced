module.exports = (sequelize, DataTypes) => {
  return sequelize.define("group", {
    name: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
  });
};
