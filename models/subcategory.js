module.exports = (sequelize, DataTypes) => {
  return sequelize.define("subcategory", {
    name: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
  });
};
