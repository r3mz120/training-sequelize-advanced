module.exports = (sequelize, DataTypes) => {
  return sequelize.define("product", {
    name: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
    expiration_date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    measure_unit: {
      type: DataTypes.STRING(2),
      allowNull: false,
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  });
};
