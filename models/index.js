const db = require("../config").db;
const Sequelize = require("sequelize");
const ProductModel = require("./product");

const UserModel = require("./user");
const CategoryModel = require("./category");
const GroupModel = require("./group");
const SubcategoryModel = require("./subcategory");

const Product = ProductModel(db, Sequelize);
const User = UserModel(db, Sequelize);
const Category = CategoryModel(db, Sequelize);
const Group = GroupModel(db, Sequelize);
const Subcategory = SubcategoryModel(db, Sequelize);

User.hasMany(Product, {
  foreignKey: { name: "ownerId", allowNull: false },
  as: "owner",
  onDelete: "cascade",
});

User.hasMany(Product, {
  foreignKey: "claimerId",
  as: "claimer",
  onDelete: "cascade",
});

User.belongsToMany(Group, { through: "Users_Groups", onDelete: "cascade" });
Group.belongsToMany(User, { through: "Users_Groups", onDelete: "cascade" });
User.hasMany(Group, {
  foreignKey: { name: "ownerId", allowNull: false, onDelete: "cascade" },
});
Category.hasMany(Product, {
  foreignKey: { allowNull: false },
  onDelete: "cascade",
});

Category.hasMany(Subcategory, {
  foreignKey: { allowNull: false },
  onDelete: "cascade",
});
Subcategory.hasMany(Product);

module.exports = {
  User,
  Product,
  Category,
  Group,
  Subcategory,
  connection: db,
};
