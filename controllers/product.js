const ProductDb = require("../models").Product;
const GroupDb = require("../models").Group;
const UserDb = require("../models").User;
const CategoryDb = require("../models").Category;
const SubcategoryDb = require("../models").Subcategory;

const controller = {
  getbyGroup: async (req, res) => {
    GroupDb.findAll({
      attributes: [],
      where: {
        id: req.params.gid,
      },
      include: {
        model: UserDb,
        attributes: ["id"],
        through: { attributes: [] },
        include: {
          model: ProductDb,
          as: "owner",
        },
      },
    })
      .then((products) => {
        if (products[0]) {
          res.status(200).send(products[0].users);
        } else res.status(401).send({ message: "no groups" });
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Database error" });
      });
  },
  updateAllById: async (req, res) => {
    ProductDb.findByPk(req.params.pid)
      .then((product) => {
        console.log(product);
        product
          .update({
            name: req.body.name,
            expDate: req.body.expDate,
            quantity: req.body.quantity,
            measure_unit: req.body.measure_unit,
            categoryId: req.body.categoryId,
            subcategoryId: req.body.subcategoryId,
          })
          .then(() => {
            res.status(201).send({ message: "accepted" });
          });
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send(err.message);
      });
  },

  addProduct: async (req, res) => {
    ProductDb.create(req.body)
      .then((product) => {
        res.status(201).send(product);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Database error" });
      });
  },
};

module.exports = controller;
