const GroupDb = require("../models").Group;
const UserDb = require("../models").User;

const controller = {
  addGroup: async (req, res) => {
    GroupDb.create({
      name: req.body.group_name,
      ownerId: req.body.owner_id,
    })
      .then(() => {
        res.status(201).send({ message: "Group created!" });
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  },

  getGroupsByOwnerId: async (req, res) => {
    GroupDb.findAll({
      where: {
        ownerId: req.params.owner_id,
      },
    })
      .then((groups) => {
        res.status(200).send(groups);
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  },
  deleteGroup: async (req, res) => {
    GroupDb.destroy({
      where: {
        ownerId: req.params.owner_id,
        id: req.params.group_id,
      },
    })
      .then(() => {
        res.status(200).send({ message: "Group deleted!" });
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  },

  addFriendInGroup: async (req, res) => {
    GroupDb.findByPk(req.body.group_id)
      .then((group) => {
        group
          .addUsers(req.body.user_id)
          .then(() => {
            res.status(200).send({ message: "Friend added to group!" });
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  },

  getFriendsFromGroup: async (req, res) => {
    GroupDb.findByPk(req.params.id, {
      attributes: [],
      include: {
        model: UserDb,
        attributes: ["id", "first_name"],
        through: {
          attributes: [],
        },
      },
    })
      .then((group) => {
        res.status(200).send(group.users);
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  },
};
module.exports = controller;
