const UserDb = require("../models").User;
const Op = require("Sequelize").Op;

const controller = {
  search: async (req, res) => {
    UserDb.findAll({
      where: {
        [Op.or]: [
          {
            first_name: { [Op.like]: `${req.params.name}%` },
          },
          {
            last_name: { [Op.like]: `${req.params.name}%` },
          },
        ],
      },
    })
      .then((user) => res.status(200).send(user))
      .catch((err) => res.status(500).send(err));
  },
  get: async (req, res) => {
    UserDb.findByPk(req.params.id)
      .then((user) => res.status(200).send(user))
      .catch((err) => {
        res.status(500).send(err);
      });
  },

  update: async (req, res) => {
    UserDb.update({ phone: req.body.phone }, { where: { id: req.body.id } })
      .then(() => {
        res.status(200).send({ message: "Phone updated!" });
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  },
};

module.exports = controller;
